import React, { Component } from 'react';
import { View, Text } from 'react-native';
import { connect } from 'react-redux'
import PushNotification from 'react-native-push-notification'

class NotifHandler extends Component {
  constructor(props) {
    super(props);
    this.state = {
    };
    const component = this;
    PushNotification.configure({
      // (required) Called when a remote or local notification is opened or received
      onNotification: function(notification) {
          console.log('NOTIFICATION:', notification );
          let user = component.props.user;

          user.item =  notification.item;
          const action = { type: "TOGGLE_ITEMDETAIL", value:user }
          component.props.dispatch(action);
      },
  
      popInitialNotification: true,
      requestPermissions: true,
    });
  }

  render() {
    return (
      <View>
      </View>
    );
  }
}

const mapStateToProps = (state) => {
    return state
  }
  const mapDispatchToProps = (dispatch) => {
    return {
      dispatch: (action) => { dispatch(action) }
    }
  }
  export default connect(mapStateToProps,mapDispatchToProps)(NotifHandler)