import React, { Component } from 'react';
import { View, Text,Button ,Image,StyleSheet,TouchableOpacity,FlatList} from 'react-native';
import {Container, Content, Card, CardItem, Icon} from 'native-base';
import {config} from '../api/firebaseConfig'
import firebase from 'firebase'
import { connect } from 'react-redux'
import Loader from '../components/Loader'
import ItemElement from '../components/ItemElement'

class ListeScreen extends Component {
    static navigationOptions = {
        title: 'Liste Item',
    };
  constructor(props) {
    super(props);
    this.state = {
      isLoading:true,
      listItems : []
    };
    this.flatListRef = ""
    this._detailItem=  this._detailItem.bind(this)
    this.redirectItemDetailAfterNotif = this.redirectItemDetailAfterNotif.bind(this);
  }

  redirectItemDetailAfterNotif(){
    if(this.props.user.item!=null){
      console.log("Exist");
      let user = this.props.user;
      let item = user.item;
      user.item=null;
      const action = { type: "TOGGLE_ITEMDETAIL", value: user }
      this.props.dispatch(action);
      this.props.navigation.navigate("Detail",{"item" : item});
    }
  }

  componentWillMount(){
      if (!firebase.apps.length) {
          firebase.initializeApp(config);
      }
      firebase.database().ref('items/'+this.props.user.id).on('value', (data) =>{
        let result = data.toJSON();
        if(result){
          let listItemFromDB = [];
          Object.keys(result).map(function(key) {
              var item = result[key];
              item.id= key;
              listItemFromDB.push(item);
          })  
          this.setState({
            listItems : listItemFromDB,
            isLoading:false
          });
        }
        this.setState({
          isLoading:false
        });
      })
  }

  componentWillUnmount() {
    clearInterval(this.interval);
  }

  componentDidMount(){
    this.interval = setInterval(() => this.setState({ time: Date.now() }), 1000);
    this.redirectItemDetailAfterNotif();
  }

  componentDidUpdate(){
    if(this.state.listItems.length>0){
      this.flatListRef.scrollToOffset({ animated: true, offset:this.state.listItems.length-1 });
    }
    this.redirectItemDetailAfterNotif();
  }

  

  _detailItem(item){
    this.props.navigation.navigate("Detail",{"item" : item});
  }

  _renderItemList(){
    if(this.state.listItems.length>0 && !this.state.isLoading){
      return(
        <FlatList 
            ref={(ref) => { this.flatListRef = ref; }}
            data={this.state.listItems}
            keyExtractor={(item)=>item.id.toString()}
            renderItem={({item}) => <ItemElement item={item} detailItem={this._detailItem} />}
        />
      );
    }else{
      if(!this.state.isLoading){
        return(
          <Text style={styles.noItem}>Aucun Item disponible</Text>
        );
      }
    }
  }

  render() {
    return (
      <Container>
          {this._renderItemList()}
          <Loader isLoading={this.state.isLoading} />
      </Container>
    );
  }
}

const styles = StyleSheet.create({
  noItem:{
    fontWeight:'bold',
    alignItems: 'center',
    justifyContent: 'center',
  }
})

const mapStateToProps = (state) => {
  return state
}
const mapDispatchToProps = (dispatch) => {
  return {
    dispatch: (action) => { dispatch(action) }
  }
}
export default connect(mapStateToProps,mapDispatchToProps)(ListeScreen)
