const initialState = { user:{} }

function toggleUserInfo(state = initialState, action) {
    let nextState
    switch (action.type) {
    case 'TOGGLE_USERINFO':
        if(state.user!=null){
          let oldUser = state.user;
          let newUser = action.value;
          newUser.item = oldUser.item;
           nextState = {
              user : newUser
          };
        }else{
          nextState = {
            user : action.value
        };
        }
        
        return nextState;
    case 'TOGGLE_ITEMDETAIL':
        nextState = {
          user : action.value
        };
      return nextState;
    default:
      return state
    }
  }

  export default toggleUserInfo